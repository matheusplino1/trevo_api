module.exports = {
  preset: 'ts-jest',
  testEnvironment: 'node',
  bail: 1,
  globals: {
    address : "http://localhost:3001",
    authToken: "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJ0ZXN0ZUBnbWFpbC5jb20iLCJpc3MiOiJ0cmV2by1hcGkiLCJpYXQiOjE1NzMyNDkxOTd9.tE9G0ZPQg2-skQpUPvxtQvisuj9Q5EIIBrq4Il2ameY"
  }
};
