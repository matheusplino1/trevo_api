import 'jest';
const request = require('supertest');

const address: String = (<any>global).address;
const auth: String = (<any>global).authToken;

test('get /pedidosprodutos', () => {
  return request(address)
    .get('/pedidosprodutos')
    .set('Authorization', auth)
    .then(response => {
      expect(response.status).toBe(200);
      expect(response.body.items).toBeInstanceOf(Array);
    }).catch(fail);
});

test('post /pedidosprodutos', () => {
  return request(address)
    .post('/pedidosprodutos')
    .set('Authorization', auth)
    .send({
      pedido: 1,
      produto: 1,
      quantidade: 1,
    }).then(response => {
      expect(response.status).toBe(200);
      expect(response.body.id).toBeDefined();
      expect(response.body.produto).toBe(1);
      expect(response.body.pedido).toBe(1);
      expect(response.body.quantidade).toBe(1);
    }).catch(fail);
});

test('post /pedidosprodutos produto invalido', () => {
  return request(address)
    .post('/pedidosprodutos')
    .set('Authorization', auth)
    .send({
      pedido: 1,
      produto: 5454,
      quantidade: 1,
    }).then(response => {
      expect(response.status).toBe(500);
    }).catch(fail);
});

test('get /pedidosprodutos/id', () => {
  return request(address)
    .get('/pedidosprodutos')
    .set('Authorization', auth)
    .then(response => {
      expect(response.body.items).toBeInstanceOf(Array);
      expect(response.body.items.length).toBeGreaterThan(0);
      response.body.items.forEach((element) => {
        request(address)
          .get(`/pedidosprodutos/${element.id}`)
          .then(result => {
            expect(result.body).toBeInstanceOf(Object);
            expect(result.body.id).toBe(element.id);
            expect(result.body._links).toBeDefined();
          });
      });
    }).catch(fail);
});

test('get /pedidosprodutos/aaaa - not found', () => {
  return request(address)
    .get('/pedidosprodutos/213123213')
    .set('Authorization', auth)
    .then(response => {
      expect(response.status).toBe(404);
    }).catch(fail);
});

test('put /pedidosprodutos/id', () => {
  return request(address)
    .post('/pedidosprodutos')
    .set('Authorization', auth)
    .send({
      pedido: 1,
      produto: 3,
      quantidade: 1,
    }).then(response => {
      return request(address).put(`/pedidosprodutos/${response.body.id}`)
                             .set('Authorization', auth)
                             .send({produto: 2})
     }).then(response => {
        expect(response.body.id).toBeDefined();
        expect(response.body.produto).toBe(2);
     }).catch(fail);
});

test('destroy /pedidosprodutos/id', () => {
  return request(address)
    .post('/pedidosprodutos')
    .set('Authorization', auth)
    .send({
      pedido: 1,
      produto: 3,
      quantidade: 1,
    }).then(response => {
      return request(address).del(`/pedidosprodutos/${response.body.id}`)
                             .set('Authorization', auth)
                             .send({})
    }).then(response => {
      expect(response.body.id).toBeUndefined();
      expect(response.body.ok).toBe(true);
    }).catch(fail);
});
