import 'jest';
const request = require('supertest');

const address: String = (<any>global).address;
const auth: String = (<any>global).authToken;

test('get /visitas', () => {
  return request(address)
    .get('/visitas')
    .set('Authorization', auth)
    .then(response => {
      expect(response.status).toBe(200);
      expect(response.body.items).toBeInstanceOf(Array);
    }).catch(fail);
});

test('post /visitas', () => {
  return request(address)
    .post('/visitas')
    .set('Authorization', auth)
    .send({
      cliente: 1,
      observacao: 'observado',
      data: Date.now(),
      visitatipo: 1,
      visitastatus: 1,
    }).then(response => {
      expect(response.status).toBe(200);
      expect(response.body.id).toBeDefined();
      expect(response.body.cliente).toBe(1);
      expect(response.body.observacao).toBe('observado');
      expect(response.body.visitatipo).toBe(1);
      expect(response.body.visitastatus).toBe(1);
    }).catch(fail);
});

test('post /visitas cliente invalido', () => {
  return request(address)
    .post('/visitas')
    .set('Authorization', auth)
    .send({
      cliente: 454,
      observacao: 'observado',
      data: Date.now(),
      visitatipo: 1,
      visitastatus: 1,
    }).then(response => {
      expect(response.status).toBe(500);
    }).catch(fail);
});

test('get /visitas/id', () => {
  return request(address)
    .get('/visitas')
    .set('Authorization', auth)
    .then(response => {
      expect(response.body.items).toBeInstanceOf(Array);
      expect(response.body.items.length).toBeGreaterThan(0);
      response.body.items.forEach((element) => {
        request(address)
          .get(`/visitas/${element.id}`)
          .then(result => {
            expect(result.body).toBeInstanceOf(Object);
            expect(result.body.id).toBe(element.id);
            expect(result.body._links).toBeDefined();
          });
      });
    }).catch(fail);
});

test('get /visitas/aaaa - not found', () => {
  return request(address)
    .get('/visitas/213123213')
    .set('Authorization', auth)
    .then(response => {
      expect(response.status).toBe(404);
    }).catch(fail);
});

test('put /visitas/id', () => {
  return request(address)
    .post('/visitas')
    .set('Authorization', auth)
    .send({
      cliente: 1,
      observacao: 'observado 2',
      data: Date.now(),
      visitatipo: 1,
      visitastatus: 1,
    }).then(response => {
      return request(address).put(`/visitas/${response.body.id}`)
                             .set('Authorization', auth)
                             .send({observacao: 'observado alterado'})
     }).then(response => {
        expect(response.body.id).toBeDefined();
        expect(response.body.observacao).toBe('observado alterado');
     }).catch(fail);
});

test('destroy /visitas/id', () => {
  return request(address)
    .post('/visitas')
    .set('Authorization', auth)
    .send({
      cliente: 1,
      observacao: 'observado 3',
      data: Date.now(),
      visitatipo: 1,
      visitastatus: 1,
    }).then(response => {
      return request(address).del(`/visitas/${response.body.id}`)
                             .set('Authorization', auth)
                             .send({})
    }).then(response => {
      expect(response.body.id).toBeUndefined();
      expect(response.body.ok).toBe(true);
    }).catch(fail);
});
