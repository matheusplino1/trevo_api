import 'jest';
const request = require('supertest');

const address: String = (<any>global).address;
const auth: String = (<any>global).authToken;

test('get /visitatipo', () => {
  return request(address)
    .get('/visitatipo')
    .set('Authorization', auth)
    .then(response => {
      expect(response.status).toBe(200);
      expect(response.body.items).toBeInstanceOf(Array);
    }).catch(fail);
});

test('post /visitatipo', () => {
  return request(address)
    .post('/visitatipo')
    .set('Authorization', auth)
    .send({
      descricao: 'Teste Tipo'
    }).then(response => {
      expect(response.status).toBe(200);
      expect(response.body.id).toBeDefined();
      expect(response.body.descricao).toBe('Teste Tipo')
    }).catch(fail);
});

test('get /visitatipo/id', () => {
  return request(address)
    .get('/visitatipo')
    .set('Authorization', auth)
    .then(response => {
      expect(response.body.items).toBeInstanceOf(Array);
      expect(response.body.items.length).toBeGreaterThan(0);
      response.body.items.forEach((element) => {
        request(address)
          .get(`/visitatipo/${element.id}`)
          .then(result => {
            expect(result.body).toBeInstanceOf(Object);
            expect(result.body.id).toBe(element.id);
            expect(result.body._links).toBeDefined();
          });
      });
    }).catch(fail);
});

test('get /visitatipo/aaaa - not found', () => {
  return request(address)
    .get('/visitatipo/213123213')
    .set('Authorization', auth)
    .then(response => {
      expect(response.status).toBe(404);
    }).catch(fail);
});

test('put /visitatipo/id', () => {
  return request(address)
    .post('/visitatipo')
    .set('Authorization', auth)
    .send({descricao: 'Tipo 2'}).then(response => {
      return request(address).put(`/visitatipo/${response.body.id}`)
                             .set('Authorization', auth)
                             .send({descricao: 'Tipo alterado'})
     }).then(response => {
        expect(response.body.id).toBeDefined();
        expect(response.body.descricao).toBe('Tipo alterado');
     }).catch(fail);
});

test('destroy /visitatipo/id', () => {
  return request(address)
    .post('/visitatipo')
    .set('Authorization', auth)
    .send({descricao: 'Tipo 3'}).then(response => {
      return request(address).del(`/visitatipo/${response.body.id}`)
                             .set('Authorization', auth)
                             .send({})
    }).then(response => {
      expect(response.body.id).toBeUndefined();
      expect(response.body.ok).toBe(true);
    }).catch(fail);
});
