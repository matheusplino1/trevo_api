import 'jest';
const request = require('supertest');

const address: String = (<any>global).address;
const auth: String = (<any>global).authToken;

test('get /perfil', () => {
  return request(address)
    .get('/perfil')
    .set('Authorization', auth)
    .then(response => {
      expect(response.status).toBe(200);
      expect(response.body.items).toBeInstanceOf(Array);
    }).catch(fail);
});

test('post /perfil', () => {
  return request(address)
    .post('/perfil')
    .set('Authorization', auth)
    .send({
      descricao: 'Teste Role'
    }).then(response => {
      expect(response.status).toBe(200);
      expect(response.body.id).toBeDefined();
      expect(response.body.descricao).toBe('Teste Role')
    }).catch(fail);
});

test('get /perfil/id', () => {
  return request(address)
    .get('/perfil')
    .set('Authorization', auth)
    .then(response => {
      expect(response.body.items).toBeInstanceOf(Array);
      expect(response.body.items.length).toBeGreaterThan(0);
      response.body.items.forEach((element) => {
        request(address)
          .get(`/perfil/${element.id}`)
          .then(result => {
            expect(result.body).toBeInstanceOf(Object);
            expect(result.body.id).toBe(element.id);
            expect(result.body._links).toBeDefined();
          });
      });
    }).catch(fail);
});

test('get /perfil/aaaa - not found', () => {
  return request(address)
    .get('/perfil/213123213')
    .set('Authorization', auth)
    .then(response => {
      expect(response.status).toBe(404);
    }).catch(fail);
});

test('put /perfil/id', () => {
  return request(address)
    .post('/perfil')
    .set('Authorization', auth)
    .send({descricao: 'perfil 2'}).then(response => {
      return request(address).put(`/perfil/${response.body.id}`)
                             .set('Authorization', auth)
                             .send({descricao: 'Perfil alterado'})
     }).then(response => {
        expect(response.body.id).toBeDefined();
        expect(response.body.descricao).toBe('Perfil alterado');
     }).catch(fail);
});

test('destroy /perfil/id', () => {
  return request(address)
    .post('/perfil')
    .set('Authorization', auth)
    .send({descricao: 'perfil 3'}).then(response => {
      return request(address).del(`/perfil/${response.body.id}`)
                             .set('Authorization', auth)
                             .send({})
    }).then(response => {
      expect(response.body.id).toBeUndefined();
      expect(response.body.ok).toBe(true);
    }).catch(fail);
});
