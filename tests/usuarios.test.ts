import 'jest';
const request = require('supertest');

const address: String = (<any>global).address;
const auth: String = (<any>global).authToken;

test('get /usuarios', () => {
  return request(address)
    .get('/usuarios')
    .set('Authorization', auth)
    .then(response => {
      expect(response.status).toBe(200);
      expect(response.body.items).toBeInstanceOf(Array);
    }).catch(fail);
});

test('post /usuarios', () => {
  return request(address)
    .post('/usuarios')
    .set('Authorization', auth)
    .send({
      nome: 'Teste Usuario',
      perfil: 1,
      email: 'usuario@teste.com',
      senha: '123456',
      logradouro: 'avenidadeij',
      cep: '23213123',
      numero: 44,
      cidade: 'Cachoeirinha',
      complemento: 'Tilápia'
    }).then(response => {
      expect(response.status).toBe(200);
      expect(response.body.id).toBeDefined();
      expect(response.body.nome).toBe('Teste Usuario');
      expect(response.body.email).toBe('usuario@teste.com');
      expect(response.body.logradouro).toBe('avenidadeij');
      expect(response.body.cep).toBe('23213123');
      expect(response.body.numero).toBe(44);
      expect(response.body.cidade).toBe('Cachoeirinha');
      expect(response.body.complemento).toBe('Tilápia');
    }).catch(fail);
});

test('post /usuarios email e perfil invalidos', () => {
  return request(address)
    .post('/usuarios')
    .set('Authorization', auth)
    .send({
      nome: 'Teste Cliente2',
      email: 'usuarioteste.com',
      senha: '123456',
      logradouro: 'avenidadeij',
      cep: '23213123',
      numero: 44,
      cidade: 'Cachoeirinha',
      complemento: 'Tilápia'
    }).then(response => {
      expect(response.status).toBe(400);
    }).catch(fail);
});

test('get /usuarios/id', () => {
  return request(address)
    .get('/usuarios')
    .set('Authorization', auth)
    .then(response => {
      expect(response.body.items).toBeInstanceOf(Array);
      expect(response.body.items.length).toBeGreaterThan(0);
      response.body.items.forEach((element) => {
        request(address)
          .get(`/usuarios/${element.id}`)
          .then(result => {
            expect(result.body).toBeInstanceOf(Object);
            expect(result.body.id).toBe(element.id);
            expect(result.body._links).toBeDefined();
          });
      });
    }).catch(fail);
});

test('get /usuarios/aaaa - not found', () => {
  return request(address)
    .get('/usuarios/213123213')
    .set('Authorization', auth)
    .then(response => {
      expect(response.status).toBe(404);
    }).catch(fail);
});

test('put /usuarios/id', () => {
  return request(address)
    .post('/usuarios')
    .set('Authorization', auth)
    .send({
      nome: 'Teste Usuario 2',
      senha: '123456',
      perfil: 1,
      email: 'usuario2@teste.com',
    }).then(response => {
      return request(address).put(`/usuarios/${response.body.id}`)
                             .set('Authorization', auth)
                             .send({nome: 'Usuario alterado'})
     }).then(response => {
        expect(response.body.id).toBeDefined();
        expect(response.body.nome).toBe('Usuario alterado');
        expect(response.body.email).toBe('usuario2@teste.com');
     }).catch(fail);
});

test('destroy /usuarios/id', () => {
  return request(address)
    .post('/usuarios')
    .set('Authorization', auth)
    .send({
      nome: 'Teste Usuario 3',
      senha: '123456',
      perfil: 1,
      email: 'usuario3@teste.com',
    }).then(response => {
      return request(address).del(`/usuarios/${response.body.id}`)
                             .set('Authorization', auth)
                             .send({})
    }).then(response => {
      expect(response.body.id).toBeUndefined();
      expect(response.body.ok).toBe(true);
    }).catch(fail);
});
