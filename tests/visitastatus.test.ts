import 'jest';
const request = require('supertest');

const address: String = (<any>global).address;
const auth: String = (<any>global).authToken;

test('get /visitastatus', () => {
  return request(address)
    .get('/visitastatus')
    .set('Authorization', auth)
    .then(response => {
      expect(response.status).toBe(200);
      expect(response.body.items).toBeInstanceOf(Array);
    }).catch(fail);
});

test('post /visitastatus', () => {
  return request(address)
    .post('/visitastatus')
    .set('Authorization', auth)
    .send({
      descricao: 'Teste Status'
    }).then(response => {
      expect(response.status).toBe(200);
      expect(response.body.id).toBeDefined();
      expect(response.body.descricao).toBe('Teste Status')
    }).catch(fail);
});

test('get /visitastatus/id', () => {
  return request(address)
    .get('/visitastatus')
    .set('Authorization', auth)
    .then(response => {
      expect(response.body.items).toBeInstanceOf(Array);
      expect(response.body.items.length).toBeGreaterThan(0);
      response.body.items.forEach((element) => {
        request(address)
          .get(`/visitastatus/${element.id}`)
          .then(result => {
            expect(result.body).toBeInstanceOf(Object);
            expect(result.body.id).toBe(element.id);
            expect(result.body._links).toBeDefined();
          });
      });
    }).catch(fail);
});

test('get /visitastatus/aaaa - not found', () => {
  return request(address)
    .get('/visitastatus/213123213')
    .set('Authorization', auth)
    .then(response => {
      expect(response.status).toBe(404);
    }).catch(fail);
});

test('put /visitastatus/id', () => {
  return request(address)
    .post('/visitastatus')
    .set('Authorization', auth)
    .send({descricao: 'Status 2'}).then(response => {
      return request(address).put(`/visitastatus/${response.body.id}`)
                             .set('Authorization', auth)
                             .send({descricao: 'Status alterado'})
     }).then(response => {
        expect(response.body.id).toBeDefined();
        expect(response.body.descricao).toBe('Status alterado');
     }).catch(fail);
});

test('destroy /visitastatus/id', () => {
  return request(address)
    .post('/visitastatus')
    .set('Authorization', auth)
    .send({descricao: 'Status 3'}).then(response => {
      return request(address).del(`/visitastatus/${response.body.id}`)
                             .set('Authorization', auth)
                             .send({})
    }).then(response => {
      expect(response.body.id).toBeUndefined();
      expect(response.body.ok).toBe(true);
    }).catch(fail);
});
