import 'jest';
const request = require('supertest');

const address: String = (<any>global).address;
const auth: String = (<any>global).authToken;

test('get /pedidos', () => {
  return request(address)
    .get('/pedidos')
    .set('Authorization', auth)
    .then(response => {
      expect(response.status).toBe(200);
      expect(response.body.items).toBeInstanceOf(Array);
    }).catch(fail);
});

test('post /pedidos', () => {
  return request(address)
    .post('/pedidos')
    .set('Authorization', auth)
    .send({
      cliente: 1,
      pedidostatus: 1,
    }).then(response => {
      expect(response.status).toBe(200);
      expect(response.body.id).toBeDefined();
      expect(response.body.cliente).toBe(1);
      expect(response.body.pedidostatus).toBe(1);
    }).catch(fail);
});

test('post /pedidos cliente invalido', () => {
  return request(address)
    .post('/pedidos')
    .set('Authorization', auth)
    .send({
      cliente: 4545,
      pedidostatus: 1,
    }).then(response => {
      expect(response.status).toBe(500);
    }).catch(fail);
});

test('get /pedidos/id', () => {
  return request(address)
    .get('/pedidos')
    .set('Authorization', auth)
    .then(response => {
      expect(response.body.items).toBeInstanceOf(Array);
      expect(response.body.items.length).toBeGreaterThan(0);
      response.body.items.forEach((element) => {
        request(address)
          .get(`/pedidos/${element.id}`)
          .then(result => {
            expect(result.body).toBeInstanceOf(Object);
            expect(result.body.id).toBe(element.id);
            expect(result.body._links).toBeDefined();
          });
      });
    }).catch(fail);
});

test('get /pedidos/aaaa - not found', () => {
  return request(address)
    .get('/pedidos/213123213')
    .set('Authorization', auth)
    .then(response => {
      expect(response.status).toBe(404);
    }).catch(fail);
});

test('put /pedidos/id', () => {
  return request(address)
    .post('/pedidos')
    .set('Authorization', auth)
    .send({
      cliente: 1,
      pedidostatus: 1,
    }).then(response => {
      return request(address).put(`/pedidos/${response.body.id}`)
                             .set('Authorization', auth)
                             .send({pedidostatus: 3})
     }).then(response => {
        expect(response.body.id).toBeUndefined();
        expect(response.status).toBe(400);
     }).catch(fail);
});

test('destroy /pedidos/id', () => {
  return request(address)
    .post('/pedidos')
    .set('Authorization', auth)
    .send({
      cliente: 1,
      pedidostatus: 1,
    }).then(response => {
      return request(address).del(`/pedidos/${response.body.id}`)
                             .set('Authorization', auth)
                             .send({})
    }).then(response => {
      expect(response.body.id).toBeUndefined();
      expect(response.body.ok).toBe(true);
    }).catch(fail);
});
