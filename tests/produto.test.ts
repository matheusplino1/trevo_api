import 'jest';
const request = require('supertest');

const address: String = (<any>global).address;
const auth: String = (<any>global).authToken;

test('get /produtos', () => {
  return request(address)
    .get('/produtos')
    .set('Authorization', auth)
    .then(response => {
      expect(response.status).toBe(200);
      expect(response.body.items).toBeInstanceOf(Array);
    }).catch(fail);
});

test('post /produtos', () => {
  return request(address)
    .post('/produtos')
    .set('Authorization', auth)
    .send({
      descricao: 'Teste Produto',
      quantidade: 3,
      preco: 200
    }).then(response => {
      expect(response.status).toBe(200);
      expect(response.body.id).toBeDefined();
      expect(response.body.descricao).toBe('Teste Produto');
      expect(response.body.quantidade).toBe(3);
    }).catch(fail);
});

test('get /produtos/id', () => {
  return request(address)
    .get('/produtos')
    .set('Authorization', auth)
    .then(response => {
      expect(response.body.items).toBeInstanceOf(Array);
      expect(response.body.items.length).toBeGreaterThan(0);
      response.body.items.forEach((element) => {
        request(address)
          .get(`/produtos/${element.id}`)
          .then(result => {
            expect(result.body).toBeInstanceOf(Object);
            expect(result.body.id).toBe(element.id);
            expect(result.body._links).toBeDefined();
          });
      });
    }).catch(fail);
});

test('get /produtos/aaaa - not found', () => {
  return request(address)
    .get('/produtos/213123213')
    .set('Authorization', auth)
    .then(response => {
      expect(response.status).toBe(404);
    }).catch(fail);
});

test('put /produtos/id', () => {
  return request(address)
    .post('/produtos')
    .set('Authorization', auth)
    .send({descricao: 'Produto 2', quantidade: 50, preco: 22.4}).then(response => {
      return request(address).put(`/produtos/${response.body.id}`)
                             .set('Authorization', auth)
                             .send({descricao: 'Produto alterado', quantidade: 40})
     }).then(response => {
        expect(response.body.id).toBeDefined();
        expect(response.body.descricao).toBe('Produto alterado');
        expect(response.body.quantidade).toBe(40);
     }).catch(fail);
});

test('destroy /produtos/id', () => {
  return request(address)
    .post('/produtos')
    .set('Authorization', auth)
    .send({descricao: 'Produto 3', quantidade: 10, preco: 140}).then(response => {
      return request(address).del(`/produtos/${response.body.id}`)
                             .set('Authorization', auth)
                             .send({})
    }).then(response => {
      expect(response.body.id).toBeUndefined();
      expect(response.body.ok).toBe(true);
    }).catch(fail);
});
