'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('Menu', [{
      id: 1,
      descricao: 'Dashboard',
      rota: 'dashboard',
      habilitado: false,
      grupo: 'usuario',
      ordem: 1,
      createdAt: new Date(),
      updatedAt: new Date()
    },{
      id: 2,
      descricao: 'Catálogo de Produtos',
      rota: 'catalogoprodutos',
      habilitado: true,
      grupo: 'usuario',
      ordem: 2,
      createdAt: new Date(),
      updatedAt: new Date()
    },{
      id: 3,
      descricao: 'Visitas',
      rota: 'visitas',
      habilitado: true,
      grupo: 'usuario',
      ordem: 3,
      createdAt: new Date(),
      updatedAt: new Date()
    },{
      id: 4,
      descricao: 'Pedidos',
      rota: 'pedidos',
      habilitado: true,
      grupo: 'usuario',
      ordem: 4,
      createdAt: new Date(),
      updatedAt: new Date()
    },{
      id: 5,
      descricao: 'Produtos',
      rota: 'produtos',
      habilitado: true,
      grupo: 'admin',
      ordem: 1,
      createdAt: new Date(),
      updatedAt: new Date()
    },{
      id: 6,
      descricao: 'Clientes',
      rota: 'clientes',
      habilitado: true,
      grupo: 'admin',
      ordem: 2,
      createdAt: new Date(),
      updatedAt: new Date()
    },{
      id: 7,
      descricao: 'Usuarios',
      rota: 'usuarios',
      habilitado: true,
      grupo: 'admin',
      ordem: 3,
      createdAt: new Date(),
      updatedAt: new Date()
    },{
      id: 8,
      descricao: 'Configurações',
      rota: 'configuracoes',
      habilitado: true,
      grupo: 'admin',
      ordem: 4,
      createdAt: new Date(),
      updatedAt: new Date()
    }], {});
  },
  down: (queryInterface, Sequelize) => {
    queryInterface.bulkDelete('Menu', null, {});
  }
};
