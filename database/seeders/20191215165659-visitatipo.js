'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('VisitaTipo', [{
      id: 1,
      descricao: 'Prospecção',
      createdAt: new Date(),
      updatedAt: new Date()
    },{
      id: 2,
      descricao: 'Rotina',
      createdAt: new Date(),
      updatedAt: new Date()
    },{
      id: 3,
      descricao: 'Entrega',
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      id: 4,
      descricao: 'Venda',
      createdAt: new Date(),
      updatedAt: new Date()
    }], {});
  },

  down: (queryInterface, Sequelize) => {
    queryInterface.bulkDelete('VisitaTipo', null, {});
  }
};
