'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('PedidoStatus', [{
      id: 1,
      descricao: 'Orçamento',
      createdAt: new Date(),
      updatedAt: new Date()
    },{
      id: 2,
      descricao: 'Pendente de aprovação',
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      id: 3,
      descricao: 'Em separação',
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      id: 4,
      descricao: 'Realizando entrega',
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      id: 5,
      descricao: 'Finalizado',
      createdAt: new Date(),
      updatedAt: new Date()
    }], {});
  },

  down: (queryInterface, Sequelize) => {
    queryInterface.bulkDelete('PedidoStatus', null, {});
  }
};
