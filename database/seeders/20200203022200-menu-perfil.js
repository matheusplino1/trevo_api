'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('MenuPerfil', [{
      id: 1,
      menu: 1,
      perfil: 1,
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      id: 2,
      menu: 2,
      perfil: 1,
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      id: 3,
      menu: 3,
      perfil: 1,
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      id: 4,
      menu: 4,
      perfil: 1,
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      id: 5,
      menu: 5,
      perfil: 1,
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      id: 6,
      menu: 6,
      perfil: 1,
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      id: 7,
      menu: 7,
      perfil: 1,
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      id: 8,
      menu: 1,
      perfil: 2,
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      id: 9,
      menu: 2,
      perfil: 2,
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      id: 10,
      menu: 3,
      perfil: 2,
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      id: 11,
      menu: 4,
      perfil: 2,
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      id: 12,
      menu: 8,
      perfil: 1,
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      id: 13,
      menu: 8,
      perfil: 2,
      createdAt: new Date(),
      updatedAt: new Date()
    }], {});
  },

  down: (queryInterface, Sequelize) => {
    queryInterface.bulkDelete('MenuPerfil', null, {});
  }
};
