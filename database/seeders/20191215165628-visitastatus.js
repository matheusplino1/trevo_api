'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('VisitaStatus', [{
      id: 1,
      descricao: 'Agendada',
      createdAt: new Date(),
      updatedAt: new Date()
    },{
      id: 2,
      descricao: 'Cancelada',
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      id: 3,
      descricao: 'Concluída',
      createdAt: new Date(),
      updatedAt: new Date()
    }], {});
  },

  down: (queryInterface, Sequelize) => {
    queryInterface.bulkDelete('VisitaStatus', null, {});
  }
};
