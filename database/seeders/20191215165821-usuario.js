'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('Usuario', [{
      nome: 'Trevo',
      perfil: 1,
      email: 'trevo@gmail.com',
      senha: '$2b$10$S5TJbEu95jxj0xYADnvHx.sCUMKZ3wPMzV.hCHuK3ITGwufycSYly',
      logradouro: 'avenidadeij',
      cep: '23213123',
      numero: 44,
      cidade: 'Cachoeirinha',
      complemento: 'Tilápia',
      createdAt: new Date(),
      updatedAt: new Date()
    },{
      nome: 'Cliente',
      perfil: 2,
      email: 'cliente@gmail.com',
      senha: '$2b$10$S5TJbEu95jxj0xYADnvHx.sCUMKZ3wPMzV.hCHuK3ITGwufycSYly',
      logradouro: 'avenidadeij',
      cep: '23213123',
      numero: 44,
      cidade: 'Cachoeirinha',
      complemento: 'Tilápia',
      createdAt: new Date(),
      updatedAt: new Date()
    }], {});
  },

  down: (queryInterface, Sequelize) => {
    queryInterface.bulkDelete('Usuario', null, {});
  }
};
