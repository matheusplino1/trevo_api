'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('Perfil', [{
      id: 1,
      descricao: 'Admin',
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      id: 2,
      descricao: 'Cliente',
      createdAt: new Date(),
      updatedAt: new Date()
    }], {});
  },

  down: (queryInterface, Sequelize) => {
    queryInterface.bulkDelete('Perfil', null, {});
  }
};
