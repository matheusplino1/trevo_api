'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Menu', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      descricao: {
        allowNull: false,
        type: Sequelize.STRING
      },
      rota: {
        allowNull: false,
        type: Sequelize.STRING
      },
      grupo: {
        allowNull: false,
        type: Sequelize.STRING
      },
      icone: {
        allowNull: true,
        type: Sequelize.STRING
      },
      ordem: {
        allowNull: false,
        type: Sequelize.INTEGER
      },
      habilitado: {
        allowNull: false,
        type: Sequelize.BOOLEAN,
        defaultValue: true
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Menu');
  }
};
