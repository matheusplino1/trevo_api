import {Server} from './server/server';
import {BootstrapRouter} from './routers';

const server = new Server();

server.bootstrap(BootstrapRouter.getRouters()).then((server) => {
	let { port, address } = server.application.address();
	address = (address == '::') ? 'localhost' : address;
	console.log(`Servidor rodando no endereco ${address}:${port}`);
}).catch(error => {
	console.log('Aconteceu algum erro');
	console.log(error);
	process.exit(1);
});
