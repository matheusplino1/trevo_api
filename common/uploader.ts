import fs from 'fs';

class UploadFile {
  private fields : Array<string>;
  private path : string;

  constructor(path = './uploads/', ...fields : string[]){
    this.fields = fields;
    this.path = path;
  }

  upload = (req, res, next) => {
    const path = this.path;
    if (!fs.existsSync(path)) {
      fs.mkdirSync(path, { recursive: true });
    }

    for (var field of this.fields){
      if(!!req.files[field]){
        const
          file = req.files[field],
          fileName = `${Date.now()}-${file.name}`;

        req.params[field] = fileName;
        req.body[field] = fileName;
        
        fs.copyFileSync(file.path, `${path}${fileName}`);
        fs.unlinkSync(file.path);
      }
    }
    next();
  }
}

const uploadFile = (path = './uploads/', ...fields : string[]) => {
  return new UploadFile(path, ...fields);
};

export default uploadFile;
