import * as restify from 'restify';
import {EventEmitter} from 'events';
import {ModelController} from '../controllers/model.controller';
import {authorize} from '../security/authz.handler';

export abstract class ModelRouter extends EventEmitter {

  basePath: String;

  constructor(protected controller: ModelController){
    super();
  }

  getMiddlewaresIndex = () => [this.controller.index];
  getMiddlewaresShow = () => [this.controller.show];
  getMiddlewaresStore = () => [authorize('1','2'), this.controller.store];
  getMiddlewaresUpdate  = () => [authorize('1','2'), this.controller.update];
  getMiddlewaresDestroy = () => [authorize('1','2'), this.controller.destroy];

  // setAuthMiddleware = (profiles, ...middlewares) => [authorize(profiles) , ...middlewares];

  applyRoutes = (application: restify.Server) => {
    application.get(`/${this.basePath}`, this.getMiddlewaresIndex());
    application.get(`/${this.basePath}/:id`, this.getMiddlewaresShow());
    application.post(`/${this.basePath}`, this.getMiddlewaresStore());
    application.put(`/${this.basePath}/:id`, this.getMiddlewaresUpdate());
    application.del(`/${this.basePath}/:id`, this.getMiddlewaresDestroy());
  };
}
