import {ModelRouter} from '../common/model.router';
import {visitasController} from '../controllers/visitas.controller';

class VisitasRouter extends ModelRouter {

  constructor(){
    super(visitasController);
    this.basePath = 'visitas';
  }

}

export const visitasRouter = new VisitasRouter();
