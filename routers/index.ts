import { mainRouter } from './main.router';
import { menuRouter } from './menu.router';
import { menuPerfilRouter } from './menuperfil.router';
import { pedidosRouter } from './pedidos.router';
import { pedidosProdutosRouter } from './pedidosprodutos.router';
import { pedidoStatusRouter } from './pedidostatus.router';
import { produtosRouter } from './produtos.router';
import { usuariosRouter } from './usuarios.router';
import { perfilRouter } from './perfil.router';
import { visitasRouter } from './visitas.router';
import { visitaStatusRouter } from './visitastatus.router';
import { visitaTipoRouter } from './visitatipo.router';

export const BootstrapRouter = {

  getRouters : () => {
    return [
      mainRouter,
      menuRouter,
      menuPerfilRouter,
      pedidosRouter,
      pedidosProdutosRouter,
      pedidoStatusRouter,
      produtosRouter,
      usuariosRouter,
      perfilRouter,
      visitasRouter,
      visitaStatusRouter,
      visitaTipoRouter,
    ];
  }

}
