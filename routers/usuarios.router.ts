import * as restify from 'restify';
import {ModelRouter} from '../common/model.router';
import {ForbiddenError} from 'restify-errors';
import {usuariosController} from '../controllers/usuarios.controller';
import {authorize} from '../security/authz.handler';

class UsuariosRouter extends ModelRouter {

  constructor(){
    super(usuariosController);
    this.basePath = 'usuarios';
  }

  authorize = () => {
    return (req, res, next) => {
      let auth = false;
      if(!!req.body && !!req.body.id){
        auth = (<any>req).authenticated.id == req.body.id;
      }

      if(auth || ((<any>req).authenticated && (<any>req).authenticated.perfil == 1)){
        next();
      } else {
        next(new ForbiddenError('Permissão Negada'));
      }
    }
  }

  getMiddlewaresStore = () => [this.authorize(), this.controller.store];
  getMiddlewaresUpdate  = () => [this.authorize(), this.controller.update];
  getMiddlewaresDestroy = () => [this.authorize(), this.controller.destroy];

  applyRoutes = (application: restify.Server) => {
    application.get(`/${this.basePath}`, this.getMiddlewaresIndex());
    application.get(`/${this.basePath}/:id`, this.getMiddlewaresShow());
    application.post(`/${this.basePath}`, this.getMiddlewaresStore());
    application.put(`/${this.basePath}/:id`, this.getMiddlewaresUpdate());
    application.del(`/${this.basePath}/:id`, this.getMiddlewaresDestroy());
    application.get(`/${this.basePath}/getUserData`, (<any>this.controller).getUserData);
  };
}

export const usuariosRouter = new UsuariosRouter();
