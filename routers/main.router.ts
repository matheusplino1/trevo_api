import * as restify from 'restify';
import { ModelRouter } from '../common/model.router';
import { mainController } from '../controllers/main.controller';
import { authenticate, getMenus } from '../security/auth.handler';

class MainRouter extends ModelRouter {

  constructor(){
    super(mainController);
  }

  applyRoutes = (application: restify.Server) => {
    application.get(`/`, this.controller.index);
    application.post(`/authenticate`, authenticate);
    application.post(`/getmenu`, getMenus)
  }

}

export const mainRouter = new MainRouter();
