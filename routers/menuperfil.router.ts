import {ModelRouter} from '../common/model.router';
import {menuPerfilController} from '../controllers/menuperfil.controller';

class MenuPerfilRouter extends ModelRouter {

  constructor(){
    super(menuPerfilController);
    this.basePath = 'menuperfil';
  }

}

export const menuPerfilRouter = new MenuPerfilRouter();
