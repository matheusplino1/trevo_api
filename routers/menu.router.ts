import {ModelRouter} from '../common/model.router';
import {menuController} from '../controllers/menu.controller';

class MenuRouter extends ModelRouter {

  constructor(){
    super(menuController);
    this.basePath = 'menu';
  }

}

export const menuRouter = new MenuRouter();
