import {ModelRouter} from '../common/model.router';
import {pedidoStatusController} from '../controllers/pedidostatus.controller';

class PedidoStatusRouter extends ModelRouter {

  constructor(){
    super(pedidoStatusController);
    this.basePath = 'pedidostatus';
  }

}

export const pedidoStatusRouter = new PedidoStatusRouter();
