import {ModelRouter} from '../common/model.router';
import {pedidosController} from '../controllers/pedidos.controller';

class PedidosRouter extends ModelRouter {

  constructor(){
    super(pedidosController);
    this.basePath = 'pedidos';
  }

}

export const pedidosRouter = new PedidosRouter();
