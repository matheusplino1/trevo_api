import {ModelRouter} from '../common/model.router';
import {visitaStatusController} from '../controllers/visitastatus.controller';

class VisitaStatusRouter extends ModelRouter {

  constructor(){
    super(visitaStatusController);
    this.basePath = 'visitastatus';
  }

}

export const visitaStatusRouter = new VisitaStatusRouter();
