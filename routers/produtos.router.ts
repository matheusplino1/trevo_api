import {ModelRouter} from '../common/model.router';
import {produtosController} from '../controllers/produtos.controller';
import {authorize} from '../security/authz.handler';
import uploadFile from '../common/uploader';

const uploader = uploadFile('./uploads/', 'thumbnail');

class ProdutosRouter extends ModelRouter {

  constructor(){
    super(produtosController);
    this.basePath = 'produtos';
  }

  getMiddlewaresStore = () => [authorize('1','2'), uploader.upload, this.controller.store];
  getMiddlewaresUpdate  = () => [authorize('1','2'), uploader.upload, this.controller.update];
}

export const produtosRouter = new ProdutosRouter();
