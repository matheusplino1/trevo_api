import {ModelRouter} from '../common/model.router';
import {visitaTipoController} from '../controllers/visitatipo.controller';

class VisitaTipoRouter extends ModelRouter {

  constructor(){
    super(visitaTipoController);
    this.basePath = 'visitatipo';
  }

}

export const visitaTipoRouter = new VisitaTipoRouter();
