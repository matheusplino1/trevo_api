import {ModelRouter} from '../common/model.router';
import {pedidosProdutosController} from '../controllers/pedidosprodutos.controller';

class PedidosProdutosRouter extends ModelRouter {

  constructor(){
    super(pedidosProdutosController);
    this.basePath = 'pedidosprodutos';
  }

}

export const pedidosProdutosRouter = new PedidosProdutosRouter();
