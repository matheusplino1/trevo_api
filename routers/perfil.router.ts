import {ModelRouter} from '../common/model.router';
import {perfilController} from '../controllers/perfil.controller';

class PerfilRouter extends ModelRouter {

  constructor(){
    super(perfilController);
    this.basePath = 'perfil';
  }

}

export const perfilRouter = new PerfilRouter();
