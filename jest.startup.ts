import { environment } from './config/environment';

environment.server.port = process.env.PORT || 3001;
environment.environment = process.env.ENVIRONMENT || 'test';

import * as jestCli from 'jest-cli';
import 'jest';

import { BootstrapRouter } from './routers';
import { Server } from './server/server';


import { Perfil, PedidoStatus, VisitaStatus, Usuario, Visita, VisitaTipo, PedidoProduto, Produto, Pedido } from './models';

import { Sequelize, Op } from "sequelize";
import {config} from './config/database';
const sequelize = new Sequelize(config.database, config.username, config.password, config);

let server: Server;

const beforeAllTests = () => {
  server = new Server();
  return server.bootstrap(BootstrapRouter.getRouters())
               // sequelize.query("UPDATE users SET y = 42 WHERE x = 12")
               .then(() => { return Visita.destroy({ where: { id : { [Op.gt]: 0} }, cascade: true, restartIdentity: true }) })
               .then(() => { return VisitaStatus.destroy({ where: { id : { [Op.gt]: 0} }, cascade: true, restartIdentity: true}) })
               .then(() => { return VisitaTipo.destroy({ where: { id : { [Op.gt]: 0} }, cascade: true, restartIdentity: true}) })
               .then(() => { return PedidoProduto.destroy({ where: { id : { [Op.gt]: 0} }, cascade: true, restartIdentity: true}) })
               .then(() => { return Produto.destroy({ where: { id : { [Op.gt]: 0} }, cascade: true, restartIdentity: true}) })
               .then(() => { return Pedido.destroy({ where: { id : { [Op.gt]: 0} }, cascade: true, restartIdentity: true}) })
               .then(() => { return PedidoStatus.destroy({where: { id : { [Op.gt]: 0} }, cascade: true, restartIdentity: true }) })
               .then(() => { return Usuario.destroy({ where: { id : { [Op.gt]: 0} }, cascade: true, restartIdentity: true }) })
               .then(() => { return Perfil.destroy({where: { id : { [Op.gt]: 0} }, cascade: true, restartIdentity: true }) })
               .then(() => {
                  let promises = [];
                  const user = {
                    "id" : 1,
                    "nome" : "teste",
                    "senha" : "teste",
                    "perfil" : 1,
                    "email" : "teste@gmail.com"
                  }, pedido = {
                    "id" : 1,
                    "cliente" : 1,
                    "pedidostatus" : 1
                  }

                  promises.push((<any>Perfil).create({ id: 1, descricao: 'Admin'}, {individualHooks: true}));
                  promises.push((<any>Perfil).create({ id: 2, descricao: 'Cliente'}, {individualHooks: true}));
                  promises.push((<any>VisitaStatus).create({id: 1, descricao: 'Visita Status Teste'}));
                  promises.push((<any>VisitaTipo).create({id: 1, descricao: 'Visita Tipo Teste'}));
                  promises.push((<any>PedidoStatus).create({id: 1, descricao: 'Pedido Status Teste'}));
                  promises.push((<any>PedidoStatus).create({id: 2, descricao: 'Pedido Status 2 Teste'}));
                  promises.push((<any>Produto).create({id: 1, descricao: 'Produto 1', quantidade: 10, preco: 100}));
                  promises.push((<any>Produto).create({id: 2, descricao: 'Produto 2', quantidade: 20, preco: 100}));
                  promises.push((<any>Produto).create({id: 3, descricao: 'Produto 3', quantidade: 30, preco: 100}));

                  return Promise.all(promises).then(() => {
                    return (<any>Usuario).create(user, {individualHooks: true}).then(() => {
                      promises.push((<any>Pedido).create(pedido, {individualHooks: true}));
                    });
                  });

               }).catch(console.error);
}

const afterAllTests = () => {
  return server.shutdown();
}

beforeAllTests()
.then(() => jestCli.run())
.then(() => afterAllTests())
.catch(console.error);
