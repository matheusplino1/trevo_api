Após realizar download do projeto, editar o arquivo config.json com os dados do banco e rodar o comando `npm install`.

Se não houver um banco de dados criado para o projeto, pode criar com o comando:

`npx sequelize-cli db:create`

Executar as migrations com o comando:

`npx sequelize-cli db:migrate`

Executar os seeders com o comando:

`npx sequelize-cli db:seed:all`

Usuário padrão criado: 

login: trevo@gmail.com

senha: trevo