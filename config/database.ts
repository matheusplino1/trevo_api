import {environment} from './environment';
import {Options} from 'sequelize';

export const config = <Options> (() => {
   let development = <Options>{
    username: 'root',
    password: 'halegria',
    database: 'controledevendas',
    host: '127.0.0.1',
    dialect: 'mysql',
    define: { freezeTableName: true }
  };

  // development = <Options>{
  //   username: 'postgres',
  //   password: '',
  //   database: 'controledevendas',
  //   host: '127.0.0.1',
  //   dialect: 'postgres',
  //   define: { freezeTableName: true }
  // };

   let test = <Options>{
     username: 'root',
     password: 'halegria',
     database: 'test',
     host: '127.0.0.1',
     dialect: 'mysql',
     define: { freezeTableName: true }
   };

   // test = <Options>{
   //   username: 'postgres',
   //   password: '',
   //   database: 'test',
   //   host: '127.0.0.1',
   //   dialect: 'postgres',
   //   define: { freezeTableName: true }
   // };

   switch (environment.environment) {
       case 'development':
           return development;

       case 'test':
           return test;

       default:
           return development;
   }
})();
