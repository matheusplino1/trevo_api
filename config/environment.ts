export const environment = {
  server: { port: process.env.SERVER_PORT || 3003 },
  security: {
    saultRounds : process.env.SAULT_ROUNDS || 10,
    apiSecret : process.env.API_SECRET || 'trevo-api-security',
    enableHTTPS : process.env.ENABLE_HTTPS || false,
    certificade : process.env.CERT_FILE || './security/keys/cert.pem',
    key : process.env.CERT_KEY_FILE || './security/keys/key.pem',
  },
  log: {
    level : process.env.LOG_LEVEL || 'debug',
    name: 'trevo-api-logger'
  },
  environment : process.env.ENVIRONMENT || 'development'
}
