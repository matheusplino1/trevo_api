import restify from 'restify';
import {ForbiddenError} from 'restify-errors';

export const authorize = function (...profiles: string[]) : restify.RequestHandler {
  return (req, res, next) => {
    if((<any>req).authenticated && profiles.some(profile => (<any>req).authenticated.perfil == profile)){
      req.log.debug(
        'User %s authorized to route %s. Required profiles: %j. User profiles: %j',
        (<any>req).authenticated.id,
        req.path(),
        profiles,
        (<any>req).authenticated.perfil
      );
      next();
    } else {
      if((<any>req).authenticated){
        (<any>req).log.debug(
          'Permissão negada para %s. Perfis solicitados: %j. Perfil: %j',
          (<any>req).authenticated.id,
          profiles,
          (<any>req).authenticated.perfil
        );
      }
      next(new  ForbiddenError('Permission Denied'));
    }
  };
}
