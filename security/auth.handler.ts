import restify from 'restify';
import * as jwt from 'jsonwebtoken';
import { NotAuthorizedError } from 'restify-errors';
import { Usuario, MenuPerfil, Menu } from '../models/index';
import { environment } from '../config/environment';

export const authenticate : restify.RequestHandler= (req, res, next) => {
  const {email, senha} = req.body;

  Usuario.findByEmail(email)
      .then(usuario => {
        if(usuario && usuario.matches(senha)){
          //gerar token
          const token = jwt.sign(
            { sub: usuario.email, iss: 'trevo-api' },
            environment.security.apiSecret
          );

          res.json({nome: usuario.nome, email: usuario.email, accessToken :  token});

          return next(false);
        } else {
          throw new NotAuthorizedError('Usuário ou senha inválidos');
        }
      }).catch(next);
}

export const getMenus : restify.RequestHandler= (req, res, next) => {
  const user = (<any>req).authenticated;

  if(!user){
    throw new NotAuthorizedError('Credenciais inválidas.');
  }

  //carrego o perfil do usuario com os menus
  (<any>MenuPerfil).findAll({where : { perfil: user.perfil }}).then(menusPerfil => {
    const promises = menusPerfil.map(menuPerfil => {
      return (<any>Menu).findOne({
        where : {
          id: menuPerfil.menu,
          habilitado: true
        }}).then(menu => {
          if(menu){
            menu.dataValues.id = undefined;
            menu.dataValues.createdAt = undefined;
            menu.dataValues.updatedAt = undefined;
            menu.dataValues.habilitado = undefined;
          }
          return menu;
        });
      });

    Promise.all(promises).then(result => {
      res.json(result.filter(obj => obj !== null));
      return next(false);
    });
  }).catch(next);
}
