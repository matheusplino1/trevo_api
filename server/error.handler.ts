import * as restify from 'restify';

export const handleError = (req: restify.Request, res: restify.Response, err, done) => {

  err.toJSON = () => {
    return {
      message : err.message,
    }
  };

  switch(err.name){
    case 'SequelizeConnectionError':
    case 'SequelizeConnectionRefusedError':
      err.statusCode = 500;
      err.toJSON = () => ({
        message: 'Erro ao conectar com banco de dados',
      });
      break;

    case 'SequelizeValidationError':
      err.statusCode = 400;
      break;

    case 'ResourceNotFoundError':
      err.statusCode = 404;
      err.toJSON = () => ({
        message: 'Rota não encontrada',
      });
      break;

      case 'NotFoundError':
        err.statusCode = 404;
        err.toJSON = () => ({
          message: 'Recurso não encontrado',
        });
        break;

    case 'ForbiddenError':
      err.statusCode = 403;
      err.toJSON = () => ({
        message: 'Usuário não autorizado',
      });
      break;

    case 'MongoError':
      if(err.code === 11000){
        err.statusCode = 400;
      }
      break;

    case 'BadRequestError':
      err.statusCode = 400;
      break;

    case 'ValidationError':
      err.statusCode = 400;
      const messages:any[] = [];
      for(let name in err.errors){
        messages.push({message : err.errors[name].message});
      }
      err.toJSON = () => ({
        message: 'Ocorreu um erro de validação ao processar sua requisição.',
        errors: messages
      });
      break;

    default:
      console.log(err.name);
      err.statusCode = 500;
      break;
  }

  done();
}
