import * as restify from 'restify';
import mongoose from 'mongoose';
import * as fs from 'fs';

import {ModelRouter} from '../common/model.router';
import {logger} from '../common/logger';
import {handleError} from './error.handler';
import {environment} from '../config/environment';
import {tokenParser} from '../security/token.parse';
const corsMiddleware = require('restify-cors-middleware');

export class Server {

	application: restify.Server

	initRoutes(routers: ModelRouter[]): Promise<any>{
		return new Promise((resolve, reject) => {
			try {

				const options : restify.ServerOptions = {
					name: 'meat-api',
					version: '1.0.0',
					log: logger
				}

				if(environment.security.enableHTTPS){
					options.certificate = fs.readFileSync(environment.security.certificade);
					options.key = fs.readFileSync(environment.security.key);
				}

				this.application = restify.createServer(options);

				this.application.pre(restify.plugins.requestLogger({
					log: logger
				}));

				const cors = corsMiddleware({
				  preflightMaxAge: 5, //Optional
				  origins: ['*'],
				  allowHeaders: ['*'],
				  exposeHeaders: ['*']
				});

				this.application.pre(cors.preflight);
				this.application.use(cors.actual);

				this.application.use(restify.plugins.queryParser());
				this.application.use(restify.plugins.bodyParser());
				this.application.use(tokenParser);
				this.application.get('/files/*', // don't forget the `/*`
					restify.plugins.serveStaticFiles('./uploads')
				);

				for(let router of routers){
					router.applyRoutes(this.application);
				}

				this.application.on('restifyError', handleError);

				this.application.listen(environment.server.port, () => {
					resolve(this.application);
				});

			} catch(e) {
				reject(e);
			}
		});
	}

	bootstrap(routers: ModelRouter[] = []): Promise<any>{
			return this.initRoutes(routers).then(() => this);
	}

	shutdown(){
		return this.application.close();
	}
}
