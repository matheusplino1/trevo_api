import * as restify from 'restify';
import { BadRequestError } from 'restify-errors';
import { Usuario } from '../models/index';
import {ModelController} from './model.controller';

class UsuariosController extends ModelController {

  constructor(){
    super(Usuario);
    this.basePath = 'usuarios';
  }

  index = (req: restify.Request, res: restify.Response, next: restify.Next)  => {
    let page = parseInt(req.query._page || 1);
    const skip = (page - 1) * this.pageSize;
    const where = {};

    page = page > 0 ? page : 1;

    if(!!req.query.perfil){
      (<any>where).perfil = req.query.perfil;
    }

    if(!!req.query.id){
      (<any>where).id = req.query.id;
    }

    this.model.findAndCountAll({
      where,
      offset: skip,
      limit: this.pageSize,
    }).then(response => {
      response.rows = response.rows.map(usuario => {
        usuario.senha = undefined;
        return usuario;
      });
      return response;
    }).then(
      this.renderAll(res, next, {
        page,
        url: req.url
      })
    ).catch(next);
  }

  getUserData = (req: restify.Request, res: restify.Response, next: restify.Next) => {
    if(!(<any>req).authenticated){
      return next(new BadRequestError('Usuário inválido'));
    }
    const usuario = (<any>Object).assign((<any>req).authenticated);
    usuario.senha = undefined;
    res.json(this.envelope(usuario));
  }
}

export const usuariosController = new UsuariosController();
