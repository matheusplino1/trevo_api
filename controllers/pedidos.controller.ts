import * as restify from 'restify';
import { Op } from 'sequelize';
import { BadRequestError } from 'restify-errors';
import { Pedido, PedidoProduto, Produto } from '../models/index';
import { ModelController } from './model.controller';

class PedidosController extends ModelController {

  private pedidoProduto;
  private produto;

  constructor(){
    super(Pedido);

    this.pedidoProduto = PedidoProduto;
    this.produto = Produto;
    this.basePath = 'pedidos';
  }

  update = (req: restify.Request, res: restify.Response, next: restify.Next) => {
    const { id } = req.params;
    const where = { id };

    if(!id){
      return new BadRequestError('Argumentos incompletos');
    }

    this.model.findOne({ where }).then( model => {
      const arrPromises = [];
      if(req.body.pedidostatus
        && model.pedidostatus != req.body.pedidostatus
        && req.body.pedidostatus > 1
      ){
        arrPromises.push(this.baixaEstoque(id));
      }

      Promise.all(arrPromises).then(() => {
        this.model.update(req.body , {
    			where,
          individualHooks: true
    		}).then(() => this.model.findOne({where})).then(model => {
          res.json(model);
        }).catch(next)
      }).catch(next);
    }).catch(next);
	}

  /**
   * Função que realiza a baixa de estoque de um pedido
  **/
  baixaEstoque = (pedido) => {
    /**
     * Consulta todos os produtos vinculados ao pedido
    **/
    return this.pedidoProduto.findAll({ where : { pedido } }).then(response => {
      const arrPromises = [];
      let produtoErro;

      if(response.length === 0){
        throw new BadRequestError('Pedido não contém produtos cadastrados!');
      }

      /**
       * Percorrer todos os produtos, verificando se tem em estoque a quantidade
       * solicitada e realiza a baixa de estoque
      **/
      return this.produto.findAll( {
        where : { id : {
          [Op.in] : response.map(pedidoProduto => pedidoProduto.produto)
        }}
      }).then(produtos => {
        /**
         * Verifico se há as quantidades do pedido em estoque
        **/
        if(produtos.some((produto: { id: number; quantidade: number; }) => {
          let produtoPedido = response.filter((pedidoProduto: { produto: number; }) => pedidoProduto.produto === produto.id);
          let quantidade = produtoPedido.reduce(function(accum: { quantidade: number; }, pedidoProduto: { quantidade: number; }){
            return accum.quantidade + pedidoProduto.quantidade;
          }, { quantidade : 0 });
          produtoErro = produto;
          return quantidade > produto.quantidade;
        })){
          throw new BadRequestError(`Nao há a quantidade do produto ${produtoErro.descricao} em estoque!`);
        }

        /**
         * Atualizo o estoque
        **/
        produtos.forEach(produto => {
          let produtoPedido = response.filter((pedidoProduto: { produto: number; }) => pedidoProduto.produto === produto.id);
          let quantidade = produtoPedido.reduce(function(accum: { quantidade: number; }, pedidoProduto: { quantidade: number; }){
            return accum.quantidade + pedidoProduto.quantidade;
          }, { quantidade : 0 });
          produto.quantidade -= quantidade;

          arrPromises.push(produto.update({ quantidade : produto.quantidade}));
        });

        return Promise.all(arrPromises);
      });
    });
  }
}

export const pedidosController = new PedidosController();
