import { Pedido } from '../models/index';
import {ModelController} from './model.controller';

class MainController extends ModelController {

  constructor(){
    super({});
  }

  index = (req, res, next) => {
    return res.json({
      _links:[
        '/authenticate',
        '/menu',
        '/menuperfil',
        '/pedidos',
        '/pedidosprodutos',
        '/pedidostatus',
        '/produtos',
        '/perfil',
        '/usuarios',
        '/visitas',
        '/visitastatus',
        '/visitatipo'
      ]
    });
  }

}

export const mainController = new MainController();
