import { Perfil } from '../models/index';
import { ModelController } from './model.controller';

class PerfilController extends ModelController {

  constructor(){
    super(Perfil);
    this.basePath = 'perfil';
  }

}

export const perfilController = new PerfilController();
