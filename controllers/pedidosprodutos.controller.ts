import * as restify from 'restify';
import { PedidoProduto } from '../models/index';
import {ModelController} from './model.controller';

class PedidosProdutosController extends ModelController {

  constructor(){
    super(PedidoProduto);
    this.basePath = 'pedidosprodutos';
  }

  index = (req: restify.Request, res: restify.Response, next: restify.Next)  => {
    let page = parseInt(req.query._page || 1);
    const skip = (page - 1) * this.pageSize;
    const where = {};

    page = page > 0 ? page : 1;
    
    if(!!req.query.produto){
      (<any>where).produto = req.query.produto;
    }

    if(!!req.query.pedido){
      (<any>where).pedido = req.query.pedido;
    }

    if(!!req.query.id){
      (<any>where).id = req.query.id;
    }

    this.model.findAndCountAll({
      where,
      offset: skip,
      limit: this.pageSize,
    }).then(
      this.renderAll(res, next, {
        page,
        url: req.url
      })
    ).catch(next);
  }
}

export const pedidosProdutosController = new PedidosProdutosController();
