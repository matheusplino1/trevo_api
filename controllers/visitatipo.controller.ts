import { VisitaTipo } from '../models/index';
import {ModelController} from './model.controller';

class VisitaTipoController extends ModelController {

  constructor(){
    super(VisitaTipo);
    this.basePath = 'visitatipo';
  }

}

export const visitaTipoController = new VisitaTipoController();
