import { MenuPerfil } from '../models/index';
import { ModelController } from './model.controller';

class MenuPerfilController extends ModelController {

  constructor(){
    super(MenuPerfil);
    this.basePath = 'menuperfil';
  }

}

export const menuPerfilController = new MenuPerfilController();
