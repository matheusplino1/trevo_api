import * as restify from 'restify';
import { BadRequestError, NotFoundError } from 'restify-errors';
import { EventEmitter } from 'events';
import { Sequelize } from "sequelize";

export abstract class ModelController extends EventEmitter {

  pageSize : number = 10;
  basePath : string;

  constructor(protected model){
    super();
  }

  envelope(document: any): any {
    let resource = (<any>Object).assign({_links:{}}, document.toJSON());
    resource._links.self = `/${this.basePath}/${resource.id}`;
    return resource;
  }

  envelopeAll(documents: any = {}, options: any = {}): any {
    const resource: any = {
      _links:{
        self: `${options.url}`
      },
      count: null,
      items: documents.rows
    }
    if(!Number.isNaN(parseInt(documents.count))){
      resource.count = +documents.count;
    }
    if(options.page && documents.count && this.pageSize){
      if(options.page > 1){
        resource._links.previous = `/${this.basePath}?_page=${options.page-1}`;
      }
      const remaining = documents.count - (options.page * this.pageSize);
      if(remaining > 0){
        resource._links.next = `/${this.basePath}?_page=${options.page+1}`;
      }
    }
    return resource;
  }

  render(response: restify.Response, next: restify.Next){
    return (document) => {
      if(document){
        this.emit('beforeRender', document);
        response.json(this.envelope(document));
      }else{
        throw new NotFoundError('Documento não encontrado');
      }
      return next(false);
    }
  }

  renderAll(response: restify.Response, next: restify.Next, options: any = {}){
    return (documents: any = {}) => {
      if(!!documents.rows){
        documents.rows.forEach((document, index, array)=>{
          this.emit('beforeRender', document);
          array[index] = this.envelope(document);
        })
        response.json(this.envelopeAll(documents, options));
      }else{
        response.json(this.envelopeAll([]));
      }
      return next(false);
    }
  }

  index = (req: restify.Request, res: restify.Response, next: restify.Next)  => {
    let
      page = parseInt(req.query._page || 1),
      pageSize = parseInt(req.query._pagesize || this.pageSize),
      where = <any>{};
    const
      skip = (page - 1) * pageSize,
      Op = (<any>Sequelize).Op;

    page = page > 0 ? page : 1;

    if(!!req.query.like){
      const
        query = req.query.like.split(','),
        coluna = query[0],
        texto = query[1];
        
      where[coluna] = { [Op.iLike]: `%${texto}%` };
    }

    this.model.findAndCountAll({
      offset: skip,
      limit: pageSize,
      order:[['id', 'ASC']],
      where
    }).then(
      this.renderAll(res, next, {
        page,
        url: req.url
      })
    ).catch(next);
	}

	show = (req: restify.Request, res: restify.Response, next: restify.Next) => {
		this.model.findOne({
			where : {id : req.params.id}
		}).then(this.render(res, next)).catch(next);
	}

	store = (req: restify.Request, res: restify.Response, next: restify.Next) => {
		this.model.create(req.body, {individualHooks: true}).then(model => {
      return res.json(model);
    }).catch(next);
	}

	update = (req: restify.Request, res: restify.Response, next: restify.Next) => {
    if(!req.params.id){
      return new BadRequestError('Argumentos incompletos');
    }

		this.model.update(req.body , {
			where: {
				id: req.params.id
			},
      individualHooks: true
		}).then(() => {
      return this.model.findOne({
  			where : {id : req.params.id}
  		})
    }).then(model => {
      res.json(model);
    }).catch(next);
	}

	destroy = (req: restify.Request, res: restify.Response, next: restify.Next) => {
		if(!req.params.id){
			return new BadRequestError('Argumentos incompletos');
		}

		this.model.destroy({
			where : {id : req.params.id}
		}).catch(next);

		return res.json({ ok: true });
	}
}
