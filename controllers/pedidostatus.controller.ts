import { PedidoStatus } from '../models/index';
import {ModelController} from './model.controller';

class PedidoStatusController extends ModelController {

  constructor(){
    super(PedidoStatus);
    this.basePath = 'pedidostatus';
  }

}

export const pedidoStatusController = new PedidoStatusController();
