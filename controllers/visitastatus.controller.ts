import { VisitaStatus } from '../models/index';
import {ModelController} from './model.controller';

class VisitaStatusController extends ModelController {

  constructor(){
    super(VisitaStatus);
    this.basePath = 'visitastatus';
  }

}

export const visitaStatusController = new VisitaStatusController();
