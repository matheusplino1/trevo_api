import { Visita } from '../models/index';
import {ModelController} from './model.controller';

class VisitasController extends ModelController {

  constructor(){
    super(Visita);
    this.basePath = 'visitas';
  }

}

export const visitasController = new VisitasController();
