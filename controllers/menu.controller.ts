import { Menu } from '../models/index';
import { ModelController } from './model.controller';

class MenuController extends ModelController {

  constructor(){
    super(Menu);
    this.basePath = 'menu';
  }

}

export const menuController = new MenuController();
