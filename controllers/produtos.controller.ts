import * as restify from 'restify';
import { BadRequestError, NotFoundError } from 'restify-errors';

import { Produto } from '../models/index';
import {ModelController} from './model.controller';

class ProdutosController extends ModelController {

  constructor(){
    super(Produto);
    this.basePath = 'produtos';
  }

  store = (req: restify.Request, res: restify.Response, next: restify.Next) => {
    const produto = (Object.keys(req.body).length === 0) ? req.params : req.body;

		this.model.create(produto, {individualHooks: true}).then(model => {
      return res.json(model);
    }).catch(next);
	}

	update = (req: restify.Request, res: restify.Response, next: restify.Next) => {
    if(!req.params.id){
      return new BadRequestError('Argumentos incompletos');
    }

		this.model.update(req.body , {
			where: {
				id: req.params.id
			},
      individualHooks: true
		}).then(() => {
      return this.model.findOne({
  			where : {id : req.params.id}
  		})
    }).then(model => {
      res.json(model);
    }).catch(next);
	}

}

export const produtosController = new ProdutosController();
