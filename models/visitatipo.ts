import { Sequelize, DataTypes } from 'sequelize';

interface VisitaTipoAttributes {
  id?: string;
  descricao: string,
}

export function initVisitaTipo(sequelize: Sequelize) {
  const attributes: SequelizeAttributes<VisitaTipoAttributes> = {
    id: {
      type: DataTypes.UUID,
      primaryKey: true,
      autoIncrement: true
    },
    descricao: {
      type: DataTypes.STRING,
      allowNull: false
    },
  };
  const VisitaTipo = sequelize.define("VisitaTipo", attributes);
  return VisitaTipo;
};
