import { Sequelize, DataTypes } from 'sequelize';

interface PedidoAttributes {
  id?: string;
  cliente: number,
  pedidostatus: number,
}

export function initPedido(sequelize: Sequelize) {
  const attributes: SequelizeAttributes<PedidoAttributes> = {
    id: {
      type: DataTypes.UUID,
      primaryKey: true,
      autoIncrement: true
    },
    cliente: {
      type: DataTypes.NUMBER,
      allowNull: false
    },
    pedidostatus: {
      type: DataTypes.NUMBER,
      allowNull: false
    },
  };
  const Pedido = sequelize.define("Pedido", attributes);
  return Pedido;
};
