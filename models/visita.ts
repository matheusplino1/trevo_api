import { Sequelize, DataTypes } from 'sequelize';

interface VisitaAttributes {
  id?: string;
  cliente: number,
  visitastatus: number,
  visitatipo: number,
  observacao: number,
  data: number,
}

export function initVisita(sequelize: Sequelize) {
  const attributes: SequelizeAttributes<VisitaAttributes> = {
    id: {
      type: DataTypes.UUID,
      primaryKey: true,
      autoIncrement: true
    },
    cliente: {
      type: DataTypes.NUMBER,
      allowNull: false
    },
    visitastatus: {
      type: DataTypes.NUMBER,
      allowNull: false
    },
    visitatipo: {
      type: DataTypes.NUMBER,
      allowNull: false
    },
    observacao: {
      type: DataTypes.STRING,
      allowNull: false
    },
    data: {
      type: DataTypes.DATE,
      allowNull: false
    },
  };
  const Visita = sequelize.define("Visita", attributes);
  return Visita;
};
