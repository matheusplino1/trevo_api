import { Sequelize, DataTypes } from 'sequelize';

interface PedidoProdutoAttributes {
  id?: string;
  pedido: number,
  produto: number,
  quantidade: number,
}

export function initPedidoProduto(sequelize: Sequelize) {
  const attributes: SequelizeAttributes<PedidoProdutoAttributes> = {
    id: {
      type: DataTypes.UUID,
      primaryKey: true,
      autoIncrement: true
    },
    pedido: {
      type: DataTypes.NUMBER,
      allowNull: false
    },
    produto: {
      type: DataTypes.NUMBER,
      allowNull: false
    },
    quantidade: {
      type: DataTypes.NUMBER,
      allowNull: false
    },
  };
  const PedidoProduto = sequelize.define("PedidoProduto", attributes);
  return PedidoProduto;
};
