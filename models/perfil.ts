import { Sequelize, DataTypes } from 'sequelize';

interface PerfilAttributes {
  id?: string;
  descricao: string,
}

export function initPerfil(sequelize: Sequelize) {
  const attributes: SequelizeAttributes<PerfilAttributes> = {
    id: {
      type: DataTypes.UUID,
      primaryKey: true,
      autoIncrement: true
    },
    descricao: {
      type: DataTypes.STRING,
      allowNull: false
    },
  };
  const Perfil = sequelize.define("Perfil", attributes);
  return Perfil;
};
