import { Sequelize, DataTypes, BuildOptions, Model } from 'sequelize';
import * as bcrypt from 'bcrypt';
import {environment} from '../config/environment';

type UsuarioAttributes =  {
  id?: string;
  perfil: number;
  nome: string,
  email: string,
  senha: string,
  logradouro?: string,
  cep?: string,
  numero?: number,
  cidade?: string,
  complemento?: string
  createdAt?: string;
  updatedAt?: string;
}

type MyModelStatic = typeof Model & {
  new (values?: object, options?: BuildOptions): UsuarioAttributes;
  findByEmail: any;
}

export function initUsuario(sequelize: Sequelize) {
  const attributes: SequelizeAttributes<UsuarioAttributes> = {
    id: {
      type: DataTypes.UUID,
      primaryKey: true,
      autoIncrement: true
    },
    perfil: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    nome: {
      type: DataTypes.STRING,
      allowNull: false
    },
    email: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        isEmail: true
      }
    },
    senha: {
      type: DataTypes.STRING,
      allowNull: false
    },
    logradouro: {
      type: DataTypes.STRING,
      allowNull: true
    },
    cep: {
      type: DataTypes.STRING,
      allowNull: true
    },
    numero: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    cidade: {
      type: DataTypes.STRING,
      allowNull: true
    },
    complemento: {
      type: DataTypes.STRING,
      allowNull: true
    },
  };
  const Usuario = <MyModelStatic>sequelize.define("Usuario", attributes);

  Usuario.prototype.matches = function (senha: string) : boolean {
    return bcrypt.compareSync(senha, this.senha);
  };

  Usuario.findByEmail = (email) => {
    return Usuario.findOne({ where : {
      email : email
    }});
  };

  const hashPassword = (object) => {
    object.senha = bcrypt.hashSync(object.senha, environment.security.saultRounds);
  }

  const updateMiddleware = function (instance){
    if(instance.changed().includes('senha')){
      hashPassword(instance);
    }
  }

  Usuario.beforeCreate((user : any) => {
    hashPassword(user);
  });

  Usuario.addHook('beforeUpdate',  function(usuario) {
    updateMiddleware(usuario);
  });

  Usuario.addHook('beforeBulkUpdate',  function(options) {
    options.individualHooks = true;
  });

  Usuario.addHook('beforeBulkCreate',  function(_records, options) {
    options.individualHooks = true;
  });

  return Usuario;
};
