'use strict';

import { Sequelize } from "sequelize";
import { initMenu } from './menu';
import { initMenuPerfil } from './menuperfil';
import { initPedido } from './pedido';
import { initPedidoProduto } from './pedidoproduto';
import { initPedidoStatus } from './pedidostatus';
import { initProduto } from './produto';
import { initUsuario } from "./usuario";
import { initPerfil } from './perfil';
import { initVisita } from "./visita";
import { initVisitaStatus } from "./visitastatus";
import { initVisitaTipo } from "./visitatipo";

import {config} from '../config/database';
// const config = require('../config/database');

const sequelize = new Sequelize(config.database, config.username, config.password, config);

export const Menu = initMenu(sequelize);
export const Pedido = initPedido(sequelize);
export const PedidoProduto = initPedidoProduto(sequelize);
export const PedidoStatus = initPedidoStatus(sequelize);
export const Produto = initProduto(sequelize);
export const Usuario = initUsuario(sequelize);
export const Perfil = initPerfil(sequelize);
export const Visita = initVisita(sequelize);
export const VisitaStatus = initVisitaStatus(sequelize);
export const VisitaTipo = initVisitaTipo(sequelize);
export const MenuPerfil = initMenuPerfil(sequelize);

const db = {
  sequelize,
  Sequelize,
  Menu: Menu,
  MenuPerfil: MenuPerfil,
  Pedido : Pedido,
  PedidoProduto : PedidoProduto,
  PedidoStatus : PedidoStatus,
  Produto : Produto,
  Usuario : Usuario,
  Perfil : Perfil,
  Visita : Visita,
  VisitaStatus : VisitaStatus,
  VisitaTipo : VisitaTipo,
};

(<any>Object).values(db).forEach((model: any) => {
  if (model.associate) {
    model.associate(db);
  }
});

export default db;
