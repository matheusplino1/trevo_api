import { Sequelize, DataTypes } from 'sequelize';

interface ProdutoAttributes {
  id?: string;
  descricao: string,
  preco: number,
  quantidade: number,
  thumbnail?: string,
}

export function initProduto(sequelize: Sequelize) {
  const attributes: SequelizeAttributes<ProdutoAttributes> = {
    id: {
      type: DataTypes.UUID,
      primaryKey: true,
      autoIncrement: true
    },
    descricao: {
      type: DataTypes.STRING,
      allowNull: false
    },
    preco: {
      type: DataTypes.DECIMAL,
      allowNull: false
    },
    quantidade: {
      type: DataTypes.NUMBER,
      allowNull: false
    },
    thumbnail: {
      type: DataTypes.STRING,
      allowNull: true
    },
  };
  const Produto = sequelize.define("Produto", attributes);
  return Produto;
};
