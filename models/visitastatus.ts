import { Sequelize, DataTypes } from 'sequelize';

interface VisitaStatusAttributes {
  id?: string;
  descricao: string,
}

export function initVisitaStatus(sequelize: Sequelize) {
  const attributes: SequelizeAttributes<VisitaStatusAttributes> = {
    id: {
      type: DataTypes.UUID,
      primaryKey: true,
      autoIncrement: true
    },
    descricao: {
      type: DataTypes.STRING,
      allowNull: false
    },
  };
  const VisitaStatus = sequelize.define("VisitaStatus", attributes);
  return VisitaStatus;
};
