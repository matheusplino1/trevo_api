import { Sequelize, DataTypes } from 'sequelize';

interface MenuPerfilAttributes {
  id?: string,
  menu: number,
  perfil: number
}

export function initMenuPerfil(sequelize: Sequelize) {
  const attributes: SequelizeAttributes<MenuPerfilAttributes> = {
    id: {
      type: DataTypes.UUID,
      primaryKey: true,
      autoIncrement: true
    },
    menu: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    perfil: {
      type: DataTypes.INTEGER,
      allowNull: false,
    }
  };
  const MenuPerfil = sequelize.define("MenuPerfil", attributes);
  return MenuPerfil;
};
