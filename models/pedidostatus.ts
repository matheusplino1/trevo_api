import { Sequelize, DataTypes } from 'sequelize';

interface PedidoStatusAttributes {
  id?: string;
  descricao: string,
}

export function initPedidoStatus(sequelize: Sequelize) {
  const attributes: SequelizeAttributes<PedidoStatusAttributes> = {
    id: {
      type: DataTypes.UUID,
      primaryKey: true,
      autoIncrement: true
    },
    descricao: {
      type: DataTypes.STRING,
      allowNull: false
    },
  };
  const PedidoStatus = sequelize.define("PedidoStatus", attributes);
  return PedidoStatus;
};
