import { Sequelize, DataTypes } from 'sequelize';

interface MenuAttributes {
  id?: string;
  descricao: string,
  rota: string,
  icone?: string,
  grupo: string,
  ordem: number,
  habilitado: boolean,
}

export function initMenu(sequelize: Sequelize) {
  const attributes: SequelizeAttributes<MenuAttributes> = {
    id: {
      type: DataTypes.UUID,
      primaryKey: true,
      autoIncrement: true
    },
    descricao: {
      type: DataTypes.STRING,
      allowNull: false
    },
    rota: {
      allowNull: false,
      type: DataTypes.STRING
    },
    grupo: {
      allowNull: false,
      type: DataTypes.STRING
    },
    icone: {
      allowNull: false,
      type: DataTypes.STRING
    },
    ordem: {
      allowNull: false,
      type: DataTypes.NUMBER
    },
    habilitado: {
      allowNull: false,
      type: DataTypes.BOOLEAN,
      defaultValue: true
    }
  };
  const Menu = sequelize.define("Menu", attributes);
  return Menu;
};
